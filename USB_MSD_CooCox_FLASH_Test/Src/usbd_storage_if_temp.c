#define STORAGE_LUN_NBR 1
#define STORAGE_BLK_NBR 256 //256 blocks * 512 = 128k
#define STORAGE_BLK_SIZ 512 //doesn't seem to work well with values other than 512

/* USER CODE BEGIN PRIVATE_DEFINES */
//In order to reserve enough flash for my drive
//I modified the linker file <projectName>.ld
//Original file: FLASH (rx) : ORIGIN = 0x8000000, LENGTH = 256K
//Modified file: FLASH (rx) : ORIGIN = 0x8000000, LENGTH = 128K
//this should limit available memory for program to 128K, leaving
//another 128K for my flash drive
#define FLASH_STORAGE_START 0x08020000
#define FLASH_STORAGE_START_PAGE ((FLASH_STORAGE_START - FLASH_BASE)/FLASH_PAGE_SIZE)
#define STORAGE_BLK_PER_PAGE (FLASH_PAGE_SIZE/STORAGE_BLK_SIZ) //for the F433 would be 4
/* USER CODE END PRIVATE_DEFINES */

/*******************************************************************************
* Function Name : STORAGE_Read_FS
* Description :
* Input : None.
* Output : None.
* Return : None.
*******************************************************************************/
int8_t STORAGE_Read_FS (uint8_t lun,
							uint8_t *buf,
							uint32_t blk_addr,
							uint16_t blk_len)
{
	/* USER CODE BEGIN 6 */
	memcpy(buf, (const void *)(FLASH_STORAGE_START + blk_addr * STORAGE_BLK_SIZ), blk_len * STORAGE_BLK_SIZ);

	return (USBD_OK);
	/* USER CODE END 6 */
}


/*******************************************************************************
* Function Name : STORAGE_Write_FS
* Description :
* Input : None.
* Output : None.
* Return : None.
*******************************************************************************/
int8_t STORAGE_Write_FS (uint8_t lun,
							uint8_t *buf,
							uint32_t blk_addr,
							uint16_t blk_len)
{
	/* USER CODE BEGIN 7 */
	FLASH_EraseInitTypeDef eraseStruct;

	eraseStruct.TypeErase = FLASH_TYPEERASE_PAGES;
	eraseStruct.PageAddress = FLASH_STORAGE_START_PAGE + blk_addr;
	eraseStruct.NbPages = 1;
	eraseStruct.Banks = 0;


	uint32_t PageError;
	uint64_t *pBuf64;
	pBuf64 = (uint64_t *) buf;

	//buffer to hold one full page
	static uint8_t pageShadow[FLASH_PAGE_SIZE];
	uint32_t targetFlashPage, lastTargetPage;
	uint32_t targetPageOffset;

	lastTargetPage = 0xFFFFFFFF; //some invalid page
	HAL_FLASH_Unlock();

	for (uint32_t blk_index = blk_addr; blk_index < (blk_addr + blk_len); blk_index++)
	{
		//copy the contents of the whole page before erasing
		targetFlashPage = FLASH_STORAGE_START_PAGE + (blk_index / STORAGE_BLK_PER_PAGE);
		targetPageOffset = (blk_index % STORAGE_BLK_PER_PAGE) * STORAGE_BLK_SIZ;

		//are we still writing to the page we wrote last?
		if (lastTargetPage != targetFlashPage)
		{
			//No, this is a different page
			//copy the contents of the page to a buffer
			memcpy(pageShadow, (const void *) (FLASH_BASE + targetFlashPage * FLASH_PAGE_SIZE), FLASH_PAGE_SIZE);

			//erase the page
			eraseStruct.PageAddress = targetFlashPage;
			eraseStruct.NbPages = 1;
			HAL_FLASHEx_Erase(&eraseStruct, &PageError);

			//no need to call FLASH_WaitForLastOperation() here
			//remember the page we're on
			lastTargetPage = targetFlashPage;
		}

		//copy one block to the buffer
		memcpy(&pageShadow[targetPageOffset], &buf[(blk_index - blk_addr) * STORAGE_BLK_SIZ], STORAGE_BLK_SIZ);

		//if we're about to change pages or this is the end, commit to Flash
		//Use DWORD (64-bit) access when programming (I expect it to be faster)
		if (((blk_index % STORAGE_BLK_PER_PAGE) == (STORAGE_BLK_PER_PAGE - 1))
				|| blk_index == (blk_addr + blk_len - 1))
		{
			pBuf64 = (uint64_t*) pageShadow;

			for (uint32_t dword_index = 0; dword_index < FLASH_PAGE_SIZE; dword_index += 8)
			{
				HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, targetFlashPage * FLASH_PAGE_SIZE + dword_index, *pBuf64);
				FLASH_WaitForLastOperation(1000); //not sure if this is required
				pBuf64++; //next 64 bits
			}
		}
	}

	//lock the flash to prevent accidental changes
	HAL_FLASH_Lock();

	return (USBD_OK);
	/* USER CODE END 7 */
}
