/**
  ******************************************************************************
  * @file           : usbd_storage_if.c
  * @brief          : Memory management layer
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
*/

/* Includes ------------------------------------------------------------------*/
#include "usbd_storage_if.h"
#include "string.h"

/* USER CODE BEGIN INCLUDE */
/* USER CODE END INCLUDE */

/** @addtogroup STM32_USB_OTG_DEVICE_LIBRARY
  * @{
  */

/** @defgroup USBD_STORAGE 
  * @brief usbd core module
  * @{
  */ 

/** @defgroup USBD_STORAGE_Private_TypesDefinitions
  * @{
  */ 
/* USER CODE BEGIN PRIVATE_TYPES */
/* USER CODE END PRIVATE_TYPES */ 
/**
  * @}
  */ 

/** @defgroup USBD_STORAGE_Private_Defines
  * @{
  */
#define FLASH_DISK_START_ADDRESS              0x0800A000     /* Flash start address */
#define FLASH_DISK_SIZE                       0x8000	//0x36000	//221184
//#define FLASH_PAGE_SIZE                       2048         /* 2K per page */


#define STORAGE_LUN_NBR                  1  
#define STORAGE_BLK_SIZ                  0x200
#define STORAGE_BLK_NBR                  (FLASH_DISK_SIZE/STORAGE_BLK_SIZ)	//0x10000
/*#define STORAGE_BLK_SIZ                  0x200*/
#define WAIT_TIMEOUT					100000


/* USER CODE BEGIN PRIVATE_DEFINES */
/* USER CODE END PRIVATE_DEFINES */
  
/**
  * @}
  */ 

/** @defgroup USBD_STORAGE_Private_Macros
  * @{
  */ 
/* USER CODE BEGIN PRIVATE_MACRO */
/* USER CODE END PRIVATE_MACRO */

/**
  * @}
  */ 

/** @defgroup USBD_STORAGE_IF_Private_Variables
  * @{
  */
/* USER CODE BEGIN INQUIRY_DATA_FS */ 
/* USB Mass storage Standard Inquiry Data */
const int8_t  STORAGE_Inquirydata_FS[] = {/* 36 */
  
  /* LUN 0 */
  0x00,		
  0x80,		
  0x02,		
  0x02,
  (STANDARD_INQUIRY_DATA_LEN - 5),
  0x00,
  0x00,	
  0x00,
  'S', 'T', 'M', ' ', ' ', ' ', ' ', ' ', /* Manufacturer : 8 bytes */
  'P', 'r', 'o', 'd', 'u', 'c', 't', ' ', /* Product      : 16 Bytes */
  ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
  '0', '.', '0' ,'1',                     /* Version      : 4 Bytes */
}; 
/* USER CODE END INQUIRY_DATA_FS */ 

uint8_t MASS_Storage[FLASH_DISK_SIZE];
FlagStatus startup = RESET;
/* USER CODE BEGIN PRIVATE_VARIABLES */
/* USER CODE END PRIVATE_VARIABLES */

/**
  * @}
  */ 
 
/** @defgroup USBD_STORAGE_IF_Exported_Variables
  * @{
  */ 
  extern USBD_HandleTypeDef hUsbDeviceFS;
/* USER CODE BEGIN EXPORTED_VARIABLES */
/* USER CODE END EXPORTED_VARIABLES */

/**
  * @}
  */ 
  
/** @defgroup USBD_STORAGE_Private_FunctionPrototypes
  * @{
  */
static int8_t STORAGE_Init_FS (uint8_t lun);
static int8_t STORAGE_GetCapacity_FS (uint8_t lun, 
                           uint32_t *block_num, 
                           uint16_t *block_size);
static int8_t  STORAGE_IsReady_FS (uint8_t lun);
static int8_t  STORAGE_IsWriteProtected_FS (uint8_t lun);
static int8_t STORAGE_Read_FS (uint8_t lun, 
                        uint8_t *buf, 
                        uint32_t blk_addr,
                        uint16_t blk_len);
static int8_t STORAGE_Write_FS (uint8_t lun, 
                        uint8_t *buf, 
                        uint32_t blk_addr,
                        uint16_t blk_len);
static int8_t STORAGE_GetMaxLun_FS (void);

/* USER CODE BEGIN PRIVATE_FUNCTIONS_DECLARATION */
/* USER CODE END PRIVATE_FUNCTIONS_DECLARATION */

/**
  * @}
  */ 
  
USBD_StorageTypeDef USBD_Storage_Interface_fops_FS =
{
  STORAGE_Init_FS,
  STORAGE_GetCapacity_FS,
  STORAGE_IsReady_FS,
  STORAGE_IsWriteProtected_FS,
  STORAGE_Read_FS,
  STORAGE_Write_FS,
  STORAGE_GetMaxLun_FS,
  (int8_t *)STORAGE_Inquirydata_FS,
};

/* Private functions ---------------------------------------------------------*/
/*******************************************************************************
* Function Name  : STORAGE_Init_FS
* Description    : 
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
int8_t STORAGE_Init_FS (uint8_t lun)
{
//	HAL_StatusTypeDef Result;
//	USBD_StatusTypeDef return_value;

//	/* USER CODE BEGIN 2 */
//	switch (lun)
//	{
//		case 0:
//			Result = HAL_FLASH_Unlock();
//		  break;
//		case 1:
//		  return USBD_FAIL;
//		default:
//		  return USBD_FAIL;
//	}
//
//	/*if (Result == HAL_OK)
//	{
//		return_value = USBD_OK;
//	}
//	else
//	{
//		return_value = USBD_FAIL;
//	}*/

	if (startup == RESET)
	{
		startup = SET;

		uint16_t i;

		for (i = 0; i < FLASH_DISK_SIZE; i++)
		{
			MASS_Storage[i] = 0xFF;
		}
	}

	return (USBD_OK);
  /* USER CODE END 2 */ 
}

/*******************************************************************************
* Function Name  : STORAGE_GetCapacity_FS
* Description    : 
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
int8_t STORAGE_GetCapacity_FS (uint8_t lun, uint32_t *block_num, uint16_t *block_size)
{
  /* USER CODE BEGIN 3 */   
  *block_num  = STORAGE_BLK_NBR;
  *block_size = STORAGE_BLK_SIZ;
  return (USBD_OK);
  /* USER CODE END 3 */ 
}

/*******************************************************************************
* Function Name  : STORAGE_IsReady_FS
* Description    : 
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
int8_t  STORAGE_IsReady_FS (uint8_t lun)
{
  /* USER CODE BEGIN 4 */ 
  return (USBD_OK);
  /* USER CODE END 4 */ 
}

/*******************************************************************************
* Function Name  : STORAGE_IsWriteProtected_FS
* Description    : 
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
int8_t  STORAGE_IsWriteProtected_FS (uint8_t lun)
{
  /* USER CODE BEGIN 5 */ 
  return (USBD_OK);
  /* USER CODE END 5 */ 
}

/*******************************************************************************
* Function Name  : STORAGE_Read_FS
* Description    : 
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
int8_t STORAGE_Read_FS (uint8_t lun, 
                        uint8_t *buf, 
                        uint32_t blk_addr,                       
                        uint16_t blk_len)
{
	  memcpy(buf,&MASS_Storage[blk_addr*STORAGE_BLK_SIZ], (blk_len*STORAGE_BLK_SIZ));

	/* USER CODE BEGIN 6 */
//	  uint16_t i, j;
	  //uint32_t ReadFlashWord;


//	  switch (lun)
//	  {
//	    case 0:
////	      for(i = 0; i < blk_len; i += 4)
////	      {
////	    	  buf[i >> 2] = ((volatile unsigned long*)(FLASH_DISK_START_ADDRESS + blk_addr))[i >> 2];
////	    	  /*ReadFlashWord = *((volatile unsigned long*)(FLASH_DISK_START_ADDRESS + blk_addr));
////	    	  buf[i >> 2] = ReadFlashWord;*/
////	      }
//	    	for(i = 0; i < blk_len; i++)
//	    	{
//	    		for (j = 0; j < 512/*STORAGE_BLK_SIZ*/; j++)
//	    		{
//	    			buf[j] = MASS_Storage[(blk_addr*512) + (i*512/*STORAGE_BLK_SIZ*/ + j)];
//	    		}
//	    	}
//
//	      break;
//	    case 1:
//	      break;
//	    default:
//	      return USBD_FAIL;
//	  }

	return (USBD_OK);
  /* USER CODE END 6 */ 
}

/*******************************************************************************
* Function Name  : STORAGE_Write_FS
* Description    :
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
int8_t STORAGE_Write_FS (uint8_t lun, 
                         uint8_t *buf, 
                         uint32_t blk_addr,
                         uint16_t blk_len)
{
	  memcpy(&MASS_Storage[blk_addr*STORAGE_BLK_SIZ],buf, (blk_len*STORAGE_BLK_SIZ));

	/*FLASH_EraseInitTypeDef PageToErase;
	uint32_t PageError;
	uint32_t WordToProgram;
	uint32_t ReadFlashWord;*/


	/* USER CODE BEGIN 7 */
//	  uint16_t i, j;
//
//	  switch (lun)
//	  {
//	    case 0:
//
//	    	for(i = 0; i < blk_len; i++)
//	    	{
//	    		//buf[i] = MASS_Storage[i];
//	    		//MASS_Storage[blk_addr + i] = buf[i];
//
//	    		for (j = 0; j < 512/*STORAGE_BLK_SIZ*/; j++)
//	    		{
//	    			MASS_Storage[(blk_addr*512) + (i*512/*STORAGE_BLK_SIZ*/ + j)] = buf[j];
//	    		}
//	    	}
//
////	      for(i = 0; i < blk_len; i += FLASH_PAGE_SIZE)
////	      {
////	        if (FLASH_WaitForLastOperation(WAIT_TIMEOUT) != HAL_TIMEOUT)
////	        {
////	        	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPERR);
////	        }
////
////	        PageToErase.TypeErase = FLASH_TYPEERASE_PAGES;
////	        PageToErase.Banks = FLASH_BANK_1;
////	        PageToErase.NbPages = 1;
////	        PageToErase.PageAddress = (FLASH_DISK_START_ADDRESS + blk_addr + i);
////
////	        //if (HAL_FLASH_Unlock() == HAL_OK)
////	        {
////		        //HAL_FLASHEx_Erase(FLASH_DISK_START_ADDRESS + blk_addr + i);
////		        HAL_FLASHEx_Erase(&PageToErase, &PageError);
////	        }
////	      }
////
////	      for(i = 0; i < blk_len; i += 4)
////	      {
////	        if(FLASH_WaitForLastOperation(WAIT_TIMEOUT) != HAL_TIMEOUT)
////	        {
////	        	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPERR);
////	        }
////	        //FLASH_ProgramWord(FLASH_DISK_START_ADDRESS + blk_addr + i , buf[i >> 2]);
////
////	        //if (HAL_FLASH_Unlock() == HAL_OK)
////	        {
////		        //WordToProgram = buf[i >> 2];
////		        HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, (FLASH_DISK_START_ADDRESS + blk_addr + i), buf[i >> 2]);
////		        //HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, (FLASH_DISK_START_ADDRESS + blk_addr + i), (uint64_t)WordToProgram);
////		        //ReadFlashWord = *((volatile unsigned long*)(FLASH_DISK_START_ADDRESS + blk_addr));
////	        }
////
////	        if (ReadFlashWord != WordToProgram)
////	        {
////	        	asm("nop");
////	        }
////
////	        if ((FLASH_DISK_START_ADDRESS + blk_addr + i) == 0x0800A200)
////	        {
////	        	asm("nop");
////	        }
////	     }
//
//	     break;
//	   case 1:
//	     break;
//	   default:
//	     return USBD_FAIL;
//	  }

	return (USBD_OK);
  /* USER CODE END 7 */ 
}

/*******************************************************************************
* Function Name  : STORAGE_GetMaxLun_FS
* Description    : 
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
int8_t STORAGE_GetMaxLun_FS (void)
{
  /* USER CODE BEGIN 8 */ 
  return (STORAGE_LUN_NBR - 1);
  /* USER CODE END 8 */ 
}

/* USER CODE BEGIN PRIVATE_FUNCTIONS_IMPLEMENTATION */
/* USER CODE END PRIVATE_FUNCTIONS_IMPLEMENTATION */

/**
  * @}
  */ 

/**
  * @}
  */  
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
